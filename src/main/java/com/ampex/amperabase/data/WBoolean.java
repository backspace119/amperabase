package com.ampex.amperabase.data;

public class WBoolean extends BaseValueWatchable<Boolean> {

    public WBoolean()
    {
        super(false);
    }

}
