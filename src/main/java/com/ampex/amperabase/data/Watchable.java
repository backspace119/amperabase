package com.ampex.amperabase.data;

public interface Watchable<T> {

    void registerWatcher(Watcher<T> watcher,boolean propOnRegister);
    void removeWatcher(Watcher<T> watcher);
    void registerSingleEventWatcher(Watcher<T> watcher,boolean propOnRegister);
    void clearWatchers();
    void prop();
}
