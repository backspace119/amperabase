package com.ampex.amperabase.data;

public interface PassthroughValueWatchable<T> extends ValueWatchable<T> {
    void setWatchable(ValueWatchable<T> watchable);
}
