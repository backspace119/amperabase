package com.ampex.amperabase.data;

@FunctionalInterface
public interface Watcher<T> {
    void update(Watchable<T> w, T val);
}
