package com.ampex.amperabase.data;

import java.util.Map;

public interface WMap<K,V> extends Watchable<V>, Map<K,V> {

}
