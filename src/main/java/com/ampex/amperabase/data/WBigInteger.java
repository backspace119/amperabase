package com.ampex.amperabase.data;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class WBigInteger extends BaseValueWatchable<BigInteger> {
    public WBigInteger()
    {
        super(BigInteger.ZERO);
    }
}
