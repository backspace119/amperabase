package com.ampex.amperabase.data;

public interface ValueWatchable<T> extends Watchable<T>{
    void set(T t);

    T get();
    void setNoProp(T t);

}
