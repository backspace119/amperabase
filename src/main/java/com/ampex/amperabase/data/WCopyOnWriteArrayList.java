package com.ampex.amperabase.data;

import java.util.concurrent.CopyOnWriteArrayList;

public class WCopyOnWriteArrayList<T> extends WArrayList<T> {
    public WCopyOnWriteArrayList()
    {
        super();
        list = new CopyOnWriteArrayList<>();
    }
}
