package com.ampex.amperabase;

import com.ampex.amperabase.AmpBuildable;
import com.ampex.amperabase.data.BaseValueWatchable;
import com.ampex.amperabase.data.WBoolean;
import com.ampex.amperabase.data.WLong;

import java.util.ArrayList;
import java.util.List;

public abstract class IConnectionManager {

    public abstract boolean isRelay();
    public abstract String getID();

    public abstract void sendPacket(AmpBuildable o);
    public abstract void disconnect();
    public abstract void received(Object o);
    public abstract void setID(String ID);
    public abstract void connected();
    public abstract IPacketProcessor getPacketProcessor();
    public abstract String getAddress();

    private List<AmpBuildable> queue = new ArrayList<>();

    public abstract BaseValueWatchable<Boolean> isConnected();

    public void queueUntilDone(AmpBuildable packet)
    {
        queue.add(packet);
    }
    public void doneDownloading()
    {
        for (AmpBuildable p : queue)
        {
            sendPacket(p);
        }
    }

    public abstract void gotHS();
    public abstract BaseValueWatchable<Long> currentLatency();

    public abstract void setCurrentLatency(long latency);

    public abstract long uptime();

    public abstract void setStartTime(long startTime);

}
