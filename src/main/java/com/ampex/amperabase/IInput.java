package com.ampex.amperabase;

import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

public interface IInput extends TXIO, IAmpByteSerializable, IAmpAmpletSerializable{
    boolean canSpend(String keys, String entropy, String prefix, boolean p2sh, KeyType keyType);

}
