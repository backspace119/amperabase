package com.ampex.amperabase;


public interface IEncryptManAPI {

    byte[] sign(byte[] toSign, KeyType type);
    String getPublicKeyString(KeyType keyType);
}
