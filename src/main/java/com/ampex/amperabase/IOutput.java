package com.ampex.amperabase;

import amp.serialization.IAmpByteSerializable;

public interface IOutput extends TXIO, IAmpByteSerializable {
    byte getVersion();
}
