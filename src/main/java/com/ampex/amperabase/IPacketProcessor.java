package com.ampex.amperabase;

public interface IPacketProcessor {


    void process(Object packet);
    IPacketGlobal getPacketGlobal();
}
