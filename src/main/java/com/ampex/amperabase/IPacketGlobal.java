package com.ampex.amperabase;

public interface IPacketGlobal {

    void cancelAllResends();

    boolean doneDownloading();

    boolean passiveConnection();
}
