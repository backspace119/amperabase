package com.ampex.amperabase;

public class InvalidAmpBuildException extends Exception {
    public InvalidAmpBuildException(String msg) {
        super(msg);
    }
}
