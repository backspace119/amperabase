package com.ampex.amperabase;

import amp.Amplet;
import com.ampex.amperabase.data.BaseValueWatchable;
import com.ampex.amperabase.data.WBigInteger;

import java.math.BigInteger;

/**
 * Created by Bryan on 7/14/2017.
 */
public interface IChainManAPI {

    public static final short POW_CHAIN = 0x0001;
    public static final short TEST_NET= 0x1110;
    int MAX_TXIOS = 100_000;
    BaseValueWatchable<BigInteger> currentHeight();

    IBlockAPI getByHeight(BigInteger height);

    BaseValueWatchable<BigInteger> getCurrentDifficulty();

    short getChainVer();

    BlockState addBlock(IBlockAPI block);

    BlockState softVerifyBlock(IBlockAPI b);

    BlockState verifyBlock(IBlockAPI b);

    void setDifficulty(BigInteger diff);

    IBlockAPI getTemp();

    IBlockAPI formBlock(BigInteger height, String ID, String merkleRoot, byte[] payload, String prevID,String solver, long timestamp, byte[] coinbase);

    IBlockAPI formBlock(Amplet amplet);
}
