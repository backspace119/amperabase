package com.ampex.amperabase;


import amp.Amplet;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;


public interface ITransManAPI {


    List<IInput> getInputsForAmountAndToken(IAddress address, BigInteger amount, Token token, boolean used);

    BigInteger getAmountInWallet(IAddress address, Token token);

    ITransAPI createTrans(String message,List<IOutput> _outputs,List<IInput> _inputs,Map<String,IKSEP> keySigMap);

    IKSEP createKSEP(String sig, String entropy,List<String> inputs, String prefix, boolean p2sh, KeyType keyType);

    IOutput createOutput(BigInteger originToPay,IAddress receiptAddress, Token token, int _index, long timestamp);

    IOutput deserializeOutput(byte[] array);
    /**
     * Method used to keep track of UTXOs that have already been used to create transactions recently. IDs are removed
     * from this list when they go on the chain
     * @return List of UTXO IDs that have already been used
     */
    List<String> getUsedUTXOs();
    /**
     * Pending transactions are kept in a list here (similar to BTC's mempool)
     * @return List of pending transactions
     */
    List<ITransAPI> getPending();

    void addUTXOs(List<IOutput> utxos);

    List<IOutput> getUTXOs(IAddress address,boolean safe);

    void undoTransaction(ITransAPI trans);

    /**
     * Used to portion out transaction verification and adding to the DB so we can parallelize the task more easily.
     * This nets quite a nice speedup as it allows for many transactions to be thrown on threads to be verified. Since
     * the DB library we're using is transactional and allows for semi-parallel read access but cannot allow for parallel
     * write (transactions have a before-after pattern, therefore to log changes correctly one must wait for another to
     * complete) we can run the verification (read from DB) all at once and then do the slower part (serial addition to the DB)
     * afterward using this method. THIS METHOD IS DANGEROUS! DO NOT USE UNLESS THE TRANSACTION YOU HAVE IS DEFINITELY
     * VALID.
     * @param transaction Transaction to add to DB
     * @return true if succeeds
     */
    boolean addTransactionNoVerify(ITransAPI transaction);

    /**
     * Verifies a transaction and then adds it to the DB if it verifies
     * @param transaction transaction to verify and add
     * @return true if transaction verifies and is successfully added
     */
    boolean addTransaction(ITransAPI transaction);


    /**
     * Verifies a transaction against current transaction state from the chain
     *
     * @param transaction Transaction to verify
     * @return true if transaction verifies
     */
    boolean verifyTransaction(ITransAPI transaction);

    /**
     * Verifies coinbase transactions. Coinbase transactions are given special privileges but are also very closely
     * monitored, this method is very sensitive to the rules and will only allow a completely valid coinbase through.
     * @param transaction coinbase transaction from block
     * @param blockHeight BigInteger height of the block
     * @param fees all transaction fees in the block added
     * @return true if valid coinbase
     */
    boolean verifyCoinbase(ITransAPI transaction, BigInteger blockHeight, BigInteger fees);

    /**
     * Used to build quick lookup DB for creating transactions. Not implemented in lite version since it receives its
     * UTXOs from the relay. This method does not actually do the processing in the current implementation. It adds
     * the block to a list and notifies a thread to begin working on it, so as to not slow down block verification.
     * @param height Block to build new state on the DB from
     * @return true if able to process
     */
    boolean postBlockProcessing(BigInteger height);


    /**
     * Verifies a coinbase transaction and adds it to the DB if it is valid
     * @param transaction coinbase transaction to verify
     * @param blockHeight BigInteger height of the block
     * @param fees all transaction fees in the block
     * @return true if valid coinbase and was able to add to DB
     */
    boolean addCoinbase(ITransAPI transaction, BigInteger blockHeight, BigInteger fees);

    void resetLite();

    ITransAPI deserializeTransaction(Amplet amp) throws InvalidTransactionException;



}
