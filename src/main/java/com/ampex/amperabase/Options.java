package com.ampex.amperabase;

/**
 * Created by Bryan on 5/10/2017.
 */
public class Options {

    public boolean relay = false;
    public boolean mining = true;
    //public int relayToUse = 0;
    public boolean nogui = false;
    public boolean testNet = false;
    public boolean dump = false;
    public boolean bDebug = false;
    public boolean mDebug = false;
    public boolean rebuild = false;
    public boolean pDebug = false;
    public boolean tDebug = false;
    public boolean lite = true;
    //public boolean tGUI = false;
    public boolean poolRelay = false;
    public boolean pool = false;

    //==============benchmarking options
    public boolean benchmark = false;
    public boolean useImpossible = false;
    public boolean useWorstCase = false;
    public int numberOfTransactions = 50_000;
    public KeyType keyType = KeyType.ED25519;
    public boolean scriptOnly = false;
    public boolean noDisk = true;

}
