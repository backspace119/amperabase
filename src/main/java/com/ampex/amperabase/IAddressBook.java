package com.ampex.amperabase;

import java.util.Map;

public interface IAddressBook {

    void add(String name,IAddress address);
    void remove(String name);
    void remove(IAddress address);

    Map<String,IAddress> getBook();

    void close();
}
