package com.ampex.amperabase;

import com.ampex.amperabase.data.WList;

import java.util.List;

public interface INetworkManagerAPI {
    boolean isRelay();
    void setLive(boolean live);
    void addRelays(List<String> relays);
    /**
     * run when we receive difficulty from the relay on a lite node. Added to protect from using a static variable
     */
    void diffSet();

    /**
     * @return true if difficulty has been set on lite node
     * @see INetworkManagerAPI#diffSet()
     */
    boolean isDiffSet();

    /**
     * wrapper for thread
     *
     * @return true if interrupted
     */
    boolean isInterrupted();

    void sendOrders(IConnectionManager connMan);

   String NET_VER = "3.0.0";

   WList<IConnectionManager> getConnections();

   List<String> getRelays();

   boolean connectionInit(String ID, IConnectionManager connMan);


    void broadcastAllBut(String ID, AmpBuildable o);

    IGlobalPacketQueuer getGPQ();

    void close();

    void broadcast(AmpBuildable o);
}
