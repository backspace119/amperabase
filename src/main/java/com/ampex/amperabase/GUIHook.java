package com.ampex.amperabase;

import javafx.application.Application;
import javafx.stage.Stage;

import java.math.BigInteger;

public interface GUIHook {

//    void setStart(BigInteger start);

    void close();

    void addTransaction(ITransAPI trans,BigInteger height);

    void pbpDone();

    void dataAdded();

//    void addShare();

//    void blockFound();

    void postInit(Application app, Stage stage);

    void updatePoolStats(long currentShares, double currentPPS, double poolFee);

//    void isMining(boolean mining);
}
