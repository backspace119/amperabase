package com.ampex.amperabase;

public interface IGlobalPacketQueuer extends Runnable {
    void enqueue(ConnManPacketPair cmpp);
}
