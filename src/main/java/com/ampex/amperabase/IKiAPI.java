package com.ampex.amperabase;

import java.math.BigInteger;

public interface IKiAPI {

    IChainManAPI getChainMan();

    ITransManAPI getTransMan();

    IEncryptManAPI getEncryptMan();

    IAddManAPI getAddMan();

    String getVersion();

    void debug(String debug);

    /**
     * Gets options the program was started with (i.e. -md for miner debug)
     *
     * @return Options object created from start of program
     * @see Options
     */
    Options getOptions();

    INetworkManagerAPI getNetMan();

    void setStartHeight(BigInteger startHeight);

    GUIHook getGUIHook();

    void downloadedTo(BigInteger height);

    /**
     * Used for doing processing after we get a block. Used currently for pool server processing (creating new workloads).
     * This method is called from the {@link IChainManAPI} normally
     *
     * @param block Block that we received and added to the chain.
     */
    void blockTick(IBlockAPI block);

    IStateManager getStateManager();

    /**
     * Method to separate the pool stuff from this, since DD packet needs to update pool height to current, this will keep us
     * from depending on ki adapter in the AmperaNet lib
     */
    void doneDownloading();

    void newTransPool();

    IAddressBook getAddressBook();


    INetworkManagerAPI getPoolNet();


    PoolData getPoolData();

    String getStringSetting(StringSettings setting);

    void setStringSetting(StringSettings setting, String value);


    boolean getSetting(Settings setting);

    void setSetting(Settings setting, boolean set);



}
