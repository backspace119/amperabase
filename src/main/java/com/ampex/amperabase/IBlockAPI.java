package com.ampex.amperabase;

import amp.serialization.IAmpAmpletSerializable;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

public interface IBlockAPI extends IAmpAmpletSerializable{

    BigInteger getHeight();
    String getPrevID();
    String getID();
    String getSolver();
    Long getTimestamp();
    byte[] getPayload();
    String getMerkleRoot();
    byte[] gpuHeader();
    String header();
    ITransAPI getCoinbase();
    ITransAPI getTransaction(String key);
    Set<String> getTransactionKeys();

    /**
     * this differs from getMerkleRoot because it actually forces a calcluation of the merkle root
     * @return calculated merkle root
     */
    String merkleRoot();
    void addTransaction(ITransAPI transaction);
}
