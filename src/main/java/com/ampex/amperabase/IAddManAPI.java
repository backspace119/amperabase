package com.ampex.amperabase;

import java.util.List;

/**
 * Created by Bryan on 8/8/2017.
 *
 * Most of the methods described in this are what the implementation is meant to do. The implementation may vary
 * slightly, but most methods should have fairly uniform execution.
 */
public interface IAddManAPI {
    /**
     * This gets a new address with the currently set entropy (default if none has been set) and the keytype given. If save
     * is true it will be saved to the address list
     *
     * @param keyType Keytype of address to generate
     * @param save    pass true to save to addresses list in DB, will always be saved to current address list in memory
     * @return newly created {@link IAddress}
     */
    IAddress getNewAdd(KeyType keyType, boolean save);

    /**
     * gets the current "main" address. This is only to keep us sane on which address we spend from when we create
     * transactions
     * @return current set "main" address
     */
    IAddress getMainAdd();

    /**
     * Gets the entropy used to create a particular address (that is ours)
     * @param a Address to get entropy for
     * @return String entropy that was used to create the address
     */
    String getEntropyForAdd(IAddress a);

    /**
     * Creates a new {@link IAddress}  with all data needed to create it passed in to this method. Easier to use than {@link IAddManAPI#getNewAdd(KeyType, boolean)}
     * since it's a one step action rather than multiple steps. This method returns a NewAdd implementation since 18.0, as well
     * as all other address creation methods here. The old Address class is only there to serve old addresses
     * that still have funds in them, and will not verify on the chain at a later date.
     * @param binOrKey Binary or Key in B64 format to use to create address
     * @param entropy String entropy used to create address
     * @param prefix 5 character String prefix to use when creating address. Current AddressManager implementation will take null or empty as no prefix
     * @param l {@link AddressLength} to use when creating the address, longer addresses are more secure against collisions, but may cost more to spend on in the future
     *                               plus they can get incredibly long and unwieldy to copy/paste
     * @param p2sh if this address is P2SH or not, if you passed in a binary, this should be true, false if else
     * @param keyType The {@link KeyType} to use to create this address. If you passed in a binary, this should be {@link KeyType#NONE}
     *                otherwise match this with the type of key you passed in
     * @return Newly created address from all args passed in
     */
    IAddress createNew(String binOrKey, String entropy, String prefix, AddressLength l, boolean p2sh, KeyType keyType);

    /**
     * Gets all addresses currently assigned to us
     * @return List of all addresses assigned to us
     */
    List<IAddress> getAll();

    IAddress createFromByteArray(byte[] array);

    IAddress decodeFromChain(String encoded);

}
