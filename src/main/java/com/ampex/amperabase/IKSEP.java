package com.ampex.amperabase;

import amp.HeadlessPrefixedAmplet;

import java.util.List;

public interface IKSEP {
    String getSig();

    String getEntropy();

    List<String> getInputs();

    String getPrefix();

    boolean isP2SH();

    KeyType getKeyType();

    void setSig(String sig);

    HeadlessPrefixedAmplet toAmplet();
}
