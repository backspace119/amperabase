package com.ampex.amperabase;

import com.ampex.amperabase.data.WHashMap;
import com.ampex.amperabase.data.WLong;
import com.ampex.amperabase.data.WMap;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PoolData {
    public IAddress payTo;
    //byte[] blockData;
    public String ID;
    public Map<BigInteger, List<String>> tracking = new HashMap<>();
    public Map<String, String> addMap = new HashMap<>();
    public WMap<String, Long> hrMap = new WHashMap<>();
    public String poolConn;
    //public List<Block> pplnsBlocks;
    public BigInteger lowestHeight = BigInteger.ZERO;
    public long timeLastFound = -1;
    public BigInteger heightLastFound = BigInteger.valueOf(-1);
    public BigInteger shareDiff = new BigInteger("00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
}
