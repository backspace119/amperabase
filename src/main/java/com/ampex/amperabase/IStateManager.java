package com.ampex.amperabase;

public interface IStateManager {

    void addBlock(IBlockAPI block, String connID);

    void start();

    void interrupt();

}
